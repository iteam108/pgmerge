# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Pg_merge ###

* Quick summary
pg_merge: utility for diff two postgres schema and update ones by diff result
* Version: 0.1
* [pg_merge homepage](https://bitbucket.org/iteam108/pgmerge)

### Installation ###

* Dependencies: Python 2.7, all needed pip  descibed in requirements.txt
pip27 install -r requirements.txt 
* Configuration: 

Main options can be descibed in settings module

settings.py:

```
#!python

#sqlite database for store all operations for rollback or view details
REGBASE="register.db"
#config database connection to "old" database
CONFIG="database.ini"
#path to dir with dumps schema and data from "old" database
#in process update syncronize with git repository described in MASTERREPO parameter
DUMPDIR_CUR="./../dumps/"
#directory for "new" schema for analyze and update
DUMPDIR_IN="./../inbox"
#git repository url for store schema and data dump
MASTERREPO=

```

Connection to "old" database schema:

database.ini

* TODO: Deployment instructions


### USAGE ###

* How run and check update

python  pg_merge.py

or make module executable

chmod +x pg_merge.py 

and run

./pg_merge.py

directory for current schema will recreate if it already exist and script will run checkout with git

After this script get current schema, current data archive and push this to remote git.
Script take file for new schema from inbox and get diff comparsion with dump old schema.
If check processing without errors diff sql be placed in dump directory.
If check procedure get error (like unsupported statements, etc), script create database with template0
in postgres and restoring in it schema dump from inbox. 
Second trying compare will run through pgquarell utility and generate diff.sql in dump directory.

TODO: Next run the update postgres db from diff.sql file.