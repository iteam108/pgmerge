import subprocess
from settings import *
import os, sys, shutil
sys.path.append("./inipgdump/")
from inipgdump import core
import logging, logging.config
import datetime
from git import *
import psycopg2
import ConfigParser
import logging
import logging.config


#init logger
logging.config.fileConfig(os.path.join(os.path.abspath("."), 'logger.conf'))
# create logger
logger = logging.getLogger('simpleLogger')
#move all stdout to logging at end
#logger.debug("Start backing up %s to %s", username, local_folder)



#TODO: describe sqllite schema

def cleanDir(dirpath):
    for tfile in os.listdir(dirpath):
        file_path = os.path.join(dirpath, tfile)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception:
            print(e)

def initRepo(folder, url):
    repo = Repo.clone_from(url, folder)
    assert not repo.bare
    return repo

def commitPushData(repo):
    repo.git.add("-A")
    repo.git.commit('-m'+'"'+datetime.datetime.now().strftime("%F_%H-%M-%S")+'"')
    repo.remotes.origin.push()




#initial DB schema
#False - not create data dump, only schema option
#TODO: need add status current and incoming for file.


#clean DUMPDIR's before processing
print "1. Cleaning working directory, prepare to processing"
if os.path.exists(DUMPDIR_CUR):
    cleanDir(DUMPDIR_CUR)
#cleanDir(DUMPDIR_IN)

#checkouts
print "2. Checkout "
repoMaster=initRepo(DUMPDIR_CUR, MASTERREPO)
#repoBranch=initRepo(DUMPDIR_IN,BRANCHREPO)

#get current data from db
if not (os.path.exists(DUMPDIR_CUR+'data')):
        os.mkdir(DUMPDIR_CUR+'data')

current_data=core.make_dump(CONFIG, DUMPDIR_CUR+'data', 0, True)


#lzma archive
print "3 : Arc the data"
subprocess.Popen('lzma -9 ' +  current_data, shell=True)
#git commit
repoMaster.git.add('--all')


print "4 : dump only schema"
current_schema=core.make_dump(CONFIG, DUMPDIR_CUR, 0, False)


print "5 : commit last data and schema"
commitPushData(repoMaster)


print "6. Try get sql diff from compare"

cmddiff = "java -jar apgdiff-2.4.jar --ignore-start-with"
inbound_file=os.path.join(DUMPDIR_IN,"nodata.sql")
resultdiff_file=os.path.join(DUMPDIR_CUR, "diff"+datetime.datetime.now().strftime("%F_%H-%M-%S"))
subprocess.Popen(cmddiff+" "+current_schema+" "+inbound_file+" > "+resultdiff_file, shell=True)


#apply dif script to master base



##Print debug block
print "-- Debug summary block ---"
print "-- Current schema file:"+current_schema
print "-- Data archive of current schema: "+current_data

print "-- Restore schema from diff"
